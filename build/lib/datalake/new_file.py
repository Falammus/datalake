#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Nov  5 11:17:22 2018

@author: christopherdev
"""

nouveau_nom = "This example works"

def fonction_etrangere_1():
    return "on appelle une fonction provenant d'un autre fichier"
    
def fonction_etrangere_2(vlr1, vlr2):
    vlr3 = sumbis(vlr1, vlr2)
    return vlr3*2
	
def sumbis(val1, val2):
	print("utilisation de sumbis")
	return val1 + val2
